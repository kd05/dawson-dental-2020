<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?> >

        <div class="blank-header"></div>
		<header>

                <div class="headerTop">
                    <div class="headerTop__wrapper">
                        <div class="headerTop__menu">
                            <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'top-nav'
                                ));
                            ?>
                        </div>
                        <div class="headerTop__phone">
                            <p class="whiteFont">CALL TODAY!</p>
                            <p><a class="blueFont boldFont" href="tel:18559997835">1-855-999-7835</a></p>
                        </div>
                        <div class="headerTop__social-icons">
                            <?php
                            wp_nav_menu(array(
                                'container'      => false,
                                'theme_location' => 'social-nav'
                            ));
                            ?>
                        </div>
                    </div>
                </div>


                <div class="headerMain">
                    <div class="headerMain__wrapper">
                        <div class="headerMain__left">
                            <a href="<?php echo site_url(); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="">
                            </a>
                        </div>

                        <div class="headerMain__right">
                            <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'main-nav'
                                ));
                            ?>

                            <div class="headerMain__mobile" id="moby-button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </div>

                    </div>
                </div>

		</header><!-- #site-header -->


        <?php  $bg_img = get_template_directory_uri()."/assets/images/content-bg.jpg";  ?>
        <div class="pageWrapper"  style="background-image: url(<?php echo $bg_img; ?>)">
            <div class="pageContentWrapper">
