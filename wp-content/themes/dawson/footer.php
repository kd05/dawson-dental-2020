<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
    </div>
    <!--pageConetentWrapper-->
            <?php
                $frontpage_id = get_option( 'page_on_front' );
                $footer_content = get_field('footer_content', $frontpage_id);
            ?>
            <div style="clear:both;"></div>
			<footer >
                <div class="footerOuterContainer__before">
                    <div class="footerOuterContainer">
                         <div class="footerTopContainer">
                             <?php echo do_shortcode($footer_content); ?>
                         </div>
                        <div class="footerBottomContainer">
                            <div class="footerMenu">
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-nav'
                                ));
                                ?>
                            </div>
                            <div class="footerSignature">
                                <p><?php echo date('Y'); ?> &#169;  Dawson Dental</p>
                                <p>Website by GeekPower <a target="_blank" href="https://www.in-toronto-web-design.ca/">Web Design In Toronto</a></p>
                            </div>
                        </div>
                    </div>
                </div>
			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>
        </div>

	</body>
</html>
