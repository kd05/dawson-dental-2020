<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function insurance_faqs_post()
{

    register_post_type('gpinsurancefaq',
        // let's now add all the options for this post type
        array('labels'              => array(
            'name'               => __('Insurance FAQs', 'dawson'), /* This is the Title of the Group */
            'singular_name'      => __('Insurance FAQ', 'dawson'), /* This is the individual type */
            'all_items'          => __('All Insurance FAQ', 'dawson'), /* the all items menu item */
            'add_new'            => __('Add New', 'dawson'), /* The add new menu item */
            'add_new_item'       => __('Add New Insurance FAQ', 'dawson'), /* Add New Display Title */
            'edit'               => __('Edit', 'dawson'), /* Edit Dialog */
            'edit_item'          => __('Edit Insurance FAQs', 'dawson'), /* Edit Display Title */
            'new_item'           => __('New Insurance FAQs', 'dawson'), /* New Display Title */
            'view_item'          => __('View Insurance FAQs', 'dawson'), /* View Display Title */
            'search_items'       => __('Search Insurance FAQs', 'dawson'), /* Search Custom Type Title */
            'not_found'          => __('Nothing found in the Database.', 'dawson'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'dawson'), /* This displays if there is nothing in the trash */
            'parent_item_colon'  => ''
        ), /* end of arrays */
            'description'         => __('These are all the store Insurance FAQs', 'dawson'), /* Custom Type Description */
            'public'              => true,
            'publicly_queryable'  => false,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'query_var'           => true,
            'menu_position'       => 29, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon'           => 'dashicons-editor-help', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) => dashicons-book */

            'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ,'page-attributes','revisions'),
            'rewrite'             => array('slug' => 'gpinsurancefaq', 'with_front' => false), /* you can specify its url slug */
            'has_archive'         => 'custom_type', /* you can rename the slug here */
            'capability_type'     => 'post',
            'hierarchical'        => false,
            /* the next one is important, it tells what's enabled in the post editor */

        ) /* end of options */
    ); /* end of register post type */



}

// adding the function to the Wordpress init
add_action('init', 'insurance_faqs_post');


