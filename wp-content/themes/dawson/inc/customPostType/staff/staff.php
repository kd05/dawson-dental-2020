<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function staffs_post()
{

    register_post_type('gp-staff',
        // let's now add all the options for this post type
        array('labels'              => array(
            'name'               => __('Staffs', 'dawson'), /* This is the Title of the Group */
            'singular_name'      => __('Staff', 'dawson'), /* This is the individual type */
            'all_items'          => __('All Staff', 'dawson'), /* the all items menu item */
            'add_new'            => __('Add New', 'dawson'), /* The add new menu item */
            'add_new_item'       => __('Add New Staff', 'dawson'), /* Add New Display Title */
            'edit'               => __('Edit', 'dawson'), /* Edit Dialog */
            'edit_item'          => __('Edit Staffs', 'dawson'), /* Edit Display Title */
            'new_item'           => __('New Staffs', 'dawson'), /* New Display Title */
            'view_item'          => __('View Staffs', 'dawson'), /* View Display Title */
            'search_items'       => __('Search Staffs', 'dawson'), /* Search Custom Type Title */
            'not_found'          => __('Nothing found in the Database.', 'dawson'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'dawson'), /* This displays if there is nothing in the trash */
            'parent_item_colon'  => ''
        ), /* end of arrays */
            'description'         => __('These are all the store Staffs', 'dawson'), /* Custom Type Description */
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'query_var'           => true,
            'menu_position'       => 29, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon'           => 'dashicons-groups', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) => dashicons-book */

            'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ,'page-attributes','revisions'),
            'rewrite'             => array('slug' => 'gp-staff', 'with_front' => false), /* you can specify its url slug */
            'capability_type'     => 'post',
            'hierarchical'        => false,
            /* the next one is important, it tells what's enabled in the post editor */

        ) /* end of options */
    ); /* end of register post type */



}

// adding the function to the Wordpress init
add_action('init', 'staffs_post');


add_theme_support( 'post-thumbnails', array( 'gp-staff') );