<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function locationSlider_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_ids' => '157',
    ), $atts );
    ob_start();

    $image_ids = $a['image_ids'];
    $sliderImages = explode(",", $image_ids);
    ?>
    <div class="locationSliderContainer"  data-aos="fade-up" >
        <div class="locationSliderContainer--before">
            <div class="locationSlider frame"  >
                <ul>
                <?php
                    foreach ($sliderImages as $sliderImage){
                        $sliderImg = current(wp_get_attachment_image_src($sliderImage, 'large-1500'));
                        ?>
                            <li class="locationSlider--single"  style="background-image: url(<?php echo $sliderImg; ?>)"></li>
                        <?php
                    }
                ?>
                </ul>
            </div>
            <div class="scrollbar common-scrollbar" >
                <div class="handle">
                    <div class="mousearea"></div>
                </div>
            </div>

            <div class="slySliderButton" >
                <i class="far fa-caret-left sly-prev"></i>
                <i class="far fa-caret-right sly-next"></i>
            </div>

        </div>

    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationSlider', 'locationSlider_shortcode' );
