<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function singleBlueBoxesPanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "image_id" => "",
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);
    ?>
    <div class="singleBlueBox">
        <div class="singleBlueBox__content" data-aos="fade-up" >
            <div class="singleBlueBox__content--inner">
                <div class="imageIcon">
                    <?php  if($main_img) { ?>  <img src="<?php echo $main_img; ?>" alt=""> <?php } ?>
                </div>
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>

    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'singleBlueBoxesPanel', 'singleBlueBoxesPanel_shortcode' );




function blueBoxesPanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "overlay_top" => "no"
    ), $atts );
    ob_start();

    $overlay_top = $a['overlay_top'];
    ?>
    <div class="blueBoxesPanelContainer  <?php if($overlay_top == "yes") { echo "blueBoxesPanelContainer--overlay"; } ?>">
        <div class="blueBoxesPanel">
            <div class="blueBoxesPanel_wrapper" >
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'blueBoxesPanel', 'blueBoxesPanel_shortcode' );



