<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

function form_wrap_shortcode($atts, $content = null) {

    $a = shortcode_atts(array(
        'image_id' => ''
    ), $atts);

    $img = $a['image_id'];
    $img_url = wp_get_attachment_url($img, "full");

    ob_start();
    ?>

    <footer>
        <div class="footerOuterContainer__before">
            <div class="footerOuterContainer non-footer-form">
                <div class="background-image" style="background-image: url(<?php echo $img_url; ?>)"></div>
                <div class="gradient"></div>
                <div class="footerTopContainer">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>
    </footer>

    <?php
    return ob_get_clean();

}
add_shortcode('formWrap', 'form_wrap_shortcode');

function form_content_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="footerContent" >
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formContent', 'form_content_shortcode' );

function form_content_left_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "button_text" => "Find Your Closest Location",
        "button_link"   => "#"
    ), $atts );
    ob_start();

    $color = "white";
    $text = $a['button_text'];
    $link = $a['button_link'];
    ?>
    <div class="footerContent__left"  data-aos="fade-right" >
        <?php echo do_shortcode($content); ?>
        <?php
        wp_nav_menu(array(
            'container'      => false,
            'theme_location' => 'social-nav'
        ));
        ?>
        <?php
        if($text) {
            echo do_shortcode("[button color='".$color."' text='".$text."' link='".$link."']");
        }
        ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formContent_left', 'form_content_left_shortcode' );





function form_content_right_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'form' => '',
    ), $atts );

    ob_start();

    ?>
    <div class="footerContent__right">
        <div class="footerContent__right--box"  data-aos="fade-left" >
            <?php echo do_shortcode($content); ?>

            <div class="footerFormContainer common-form form">
                <?php // echo do_shortcode('[contact-form-7 id="268" title="Footer - Contact Form"]'); ?>

                <?php echo do_shortcode('[wpforms id="19205"]'); ?>

            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'formContent_right', 'form_content_right_shortcode' );