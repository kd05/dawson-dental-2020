<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function announcement_shortcode( $atts, $content = null ) {
    $a= array(
        "icon" => "fal fa-info-circle",
        'image_id' => ''
    );
    $a = shortcode_atts($a, $atts);


    $icon = $a['icon'];
    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();

    ?>
    <div class="announcementContainer">
        <div class="announcementContainer--inner"  style="background-image: url(<?php echo $main_img; ?>)">
            <div class="announcementContainer__gradient"></div>
            <div class="announcementContainer__content">
                <i class="<?php echo $icon; ?>"></i>
                <div class="announcementContainer__content-wrapper"  data-aos="fade-up" >
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'announcement', 'announcement_shortcode' );