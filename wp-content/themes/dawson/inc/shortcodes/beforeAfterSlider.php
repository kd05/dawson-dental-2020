<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function beforeAfterSlider_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'slider_name' => 'beforeAfterSlider',
        'ids' => '255',
        'animation' => 'yes'
    ), $atts );
    ob_start();

    $ids = $a['ids'];
    $sliderIds = explode(",", $ids);
    $sliderName = $a['slider_name'];
    $sliderImg = current(wp_get_attachment_image_src(44, 'large-1500'));

    $animation = $a['animation'];
    $animationCode = ($animation == "yes") ? 'data-aos="fade-up"' : '';

    ?>
    <div class="beforeAfterSliderContainer" <?php echo $animationCode; ?>>
        <div class="beforeAfterSliderContainer--abs" style="background-image: url(<?php echo $sliderImg; ?>)"><div class="beforeAfterSliderContainer--abs--overlay"></div></div>

        <?php
        $content = do_shortcode($content);
        echo do_shortcode("[centerContent]".$content."[/centerContent]");
        ?>
            <div class="<?php echo $sliderName; ?> frame" >
                <ul>
                    <?php
                    $sliderCount = 0;
                    foreach ($sliderIds as $sliderId){
                        $name = get_the_title($sliderId);
                        $age = get_field("ss_age",$sliderId);
                        $before_image_thumbnail = (get_field("before_image",$sliderId))['sizes']['medium-700'];

                        $after_image_thumbnail = (get_field("after_image",$sliderId))['sizes']['medium-700'];
                        $after_image_full = (get_field("after_image",$sliderId))['sizes']['large-1500'];
                        ?>
                        <li class="beforeAfterSlider--single beforeAfterSlider--single--<?php echo $sliderCount; ?>" data-bg-img="<?php echo $after_image_full; ?>">
                            <div class="beforeAfterSlider--single__content">
                                <div class="ba-img before-img" style="background-image: url(<?php echo $before_image_thumbnail; ?>)">
                                    <div class="ba-img-overlay"></div><h4>Before</h4>
                                </div>
                                <div class="ba-img after-img" style="background-image: url(<?php echo $after_image_thumbnail; ?>)">
                                    <div class="ba-img-overlay"></div><h4>After</h4>
                                </div>
                            </div>

<!--                            <div class="beforeAfterSlider--patientInfo">-->
<!--                                <p class="patientInfo--name">--><?php //echo $name; ?><!--</p>-->
<!--                                <p class="patientInfo--age">Age --><?php //echo $age; ?><!--</p>-->
<!--                            </div>-->

                        </li>
                        <?php $sliderCount++;
                    }
                    ?>
                </ul>
            </div>
            <div class="scrollbar common-scrollbar"  >
                <div class="handle">
                    <div class="mousearea"></div>
                </div>
            </div>

    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'beforeAfterSlider', 'beforeAfterSlider_shortcode' );


