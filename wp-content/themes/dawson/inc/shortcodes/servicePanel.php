<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function service_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "button_text" => "Check out all our services",
            "button_link"   => "#"
    ), $atts );
    ob_start();

    $color = "yellow";
    $text = $a['button_text'];
    $link = $a['button_link'];
    ?>
    <div class="servicePanelContainer ">
        <div class="servicePanelContainer__wrapper">
            <div class="servicePanelContainer__left" data-aos="fade-right">
                <?php echo do_shortcode($content); ?>
            </div>
            <div class="servicePanelContainer__right" data-aos="fade-left">
                <div class="servicePanelContainer__right--inner">
                    <?php echo do_shortcode("[button color='".$color."' text='".$text."' link='".$link."']");  ?>
                </div>
            </div>

            <div class="servicePanelContainer__bottomBlue-before" >
                <div class="servicePanelContainer__bottomBlue">
                    <div class="servicePanelContainer_services">
                        <?php
                        $args = [
                            "post_type" => "gp-service",
                            "posts_per_page" => 6,
                            "post_parent" => 0,
                            'fields' => 'ids',
                            'orderby' => 'menu_order',
                            'order' => 'ASC'
                        ];
                        $services = get_posts($args);
                        $animation_time = 500;
                        foreach ($services as $serviceId){
                            $serviceTitle = get_the_title($serviceId);
                            $svgId = get_field('svg_icon', $serviceId);
                            $svgIconPath = get_svg_path( $svgId);
                            $serviceLink = get_the_permalink($serviceId);
                            ?>
                            <div class="services-single" data-aos="fade-up" data-aos-duration="<?php echo $animation_time; ?>">
                                <div class="services-single__inner">
                                    <div class="svgIcon">
                                        <?php  if($svgIconPath) require_once($svgIconPath);  ?>
                                    </div>
                                    <h5><?php echo $serviceTitle; ?></h5>
                                    <a href="<?php echo $serviceLink; ?>" class="service-link"></a>
                                </div>
                            </div>
                         <?php $animation_time = $animation_time + 400;
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'servicePanel', 'service_panel_shortcode' );
