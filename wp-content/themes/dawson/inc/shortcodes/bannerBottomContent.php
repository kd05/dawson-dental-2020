<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function banner_bottom_content_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "icon" => ""
    ), $atts );
    ob_start();

    $icon = $a['icon'];
    ?>
    <div class="bannerBottomContainer " data-aos="fade-right">
        <div class="bannerBottomContainer__blue--before">
            <div class="bannerBottomContainer__blue" >
                <div class="bannerBottomContainer__blue__wrapper">
                    <?php if(!empty($icon)) { echo '<i class="'.$icon.'"></i>'; } ?>
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'bannerBottomContent', 'banner_bottom_content_shortcode' );





function locationIcons_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="locationIconsContainer">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationIcons', 'locationIcons_shortcode' );





function locationSingleIcon_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "title" => "",
        "icon" => "",
        "link" => "",
    ), $atts );
    ob_start();
    $title = $a['title'];
    $icon = $a['icon'];
    $link = $a['link'];
    ?>
    <div class="locationIcon">
        <div class="locationIcon__inner">
            <div class="locationIcon__icon">
                <?php if(!empty($icon)) { echo '<i class="'.$icon.'"></i>'; } ?>
            </div>
            <a href="<?php echo $link; ?>"><h5><?php echo $title; ?></h5></a>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationSingleIcon', 'locationSingleIcon_shortcode' );










function bannerBottomContentTwo_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="bannerBottomContainerTwo " data-aos="fade-right">
        <div class="bannerBottomContainerTwo--inner" >
                <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'bannerBottomContentTwo', 'bannerBottomContentTwo_shortcode' );