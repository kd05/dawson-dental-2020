<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function center_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'width' => '1180px',
        'padding' => '40px 30px',
        'content_align' => 'left',
        'animation' => 'no',
        'extra_class' => ''
    ), $atts );
    ob_start();

    $maxWidth = $a['width'];
    $padding = $a['padding'];
    $content_align = "align_".$a['content_align'];
    $animation = $a['animation'];
    $extraClass = $a['extra_class'];
    ?>
    <div class="centerPanelContainer <?php echo $extraClass; ?>" >
        <div class="centerPanel <?php echo $content_align; ?>" style="max-width: <?php echo $maxWidth; ?>; padding: <?php echo $padding; ?>; " <?php if($animation == "yes") { echo ' data-aos="fade-up" ';} ?>>
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'centerPanel', 'center_panel_shortcode' );




//Not used
function center_content_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();

    ?>
    <div class="centerContentContainer">
        <div class="centerContent" >
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'centerContent', 'center_content_shortcode' );