<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function servicesFourColumnBox_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "service_ids" => "65,66,67,68",
    ), $atts );
    ob_start();

    $service_ids = $a['service_ids'];
    $serviceIds = explode(",", $service_ids);
    ?>
    <div class="servicesFourColumnBoxContainer">
        <div class="servicesFourColumnBox">

            <?php
            $page_header = do_shortcode($content);
            echo do_shortcode("[centerContent]".$page_header."[/centerContent]")
            ?>

            <div class="servicesFourColumnBox_wrapper" >
                <?php
                $animation_time = 400;
                foreach ($serviceIds as $serviceId){
                    $serviceTitle = get_the_title($serviceId);
                    $svgId = get_field('svg_icon', $serviceId);
                    $svgIconPath = get_svg_path( $svgId);
                    $serviceLink = get_the_permalink($serviceId);
                    ?>
                    <div class="singleBlueBox">
                        <div class="singleBlueBox__content" data-aos="fade-up"  data-aos-duration="<?php echo $animation_time; ?>">
                            <div class="singleBlueBox__content--inner">
                                <div class="svgIcon">
                                    <?php  if($svgIconPath) include($svgIconPath);  ?>
                                </div>
                                <p><?php echo $serviceTitle; ?></p>
                                <?php echo do_shortcode("[button color='white' text='Learn More' link='".$serviceLink."']");  ?>
                            </div>
                        </div>
                    </div>
                    <?php $animation_time += 300;
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'servicesFourColumnBox', 'servicesFourColumnBox_shortcode' );


