<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function popupForm_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <!--Creates the popup body-->
    <div class="popupForm-overlay">
        <!--Creates the popup content-->
        <div class="popupForm-content">
            <h4 class="formTitle"></h4>

            <?php // echo do_shortcode('[contact-form-7 id="18977" title="Careers"]'); ?>
            <?php  echo do_shortcode('[contact-form-7 id="18984" title="Careers Individual"]'); ?>


            <!--popup's close button-->
            <div class="closePopup"><i class="fas fa-times-circle"></i></div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'popupForm', 'popupForm_shortcode' );
