<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function accordionContainer_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="accordionContainer" >
          <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'accordionContainer', 'accordionContainer_shortcode' );




function singleAccordion_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "title" => "No Title"
    ), $atts );
    ob_start();
    ?>
    <div class="accordionContainer" >
        <div class="commonAccordionContainer">
            <div class="accordionTitle">
                <h4><?php echo $a['title']; ?></h4>
            </div>
            <div class="accordionContent">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleAccordion', 'singleAccordion_shortcode' );