<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function locationMap_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
        <div class="locationMapContainer">
            <div class="locationMap">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'locationMap', 'locationMap_shortcode' );
