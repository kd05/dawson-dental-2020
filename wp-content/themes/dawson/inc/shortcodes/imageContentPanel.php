<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function background_image_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);

    ?>
    <div class="backgroundImagePanel"  style="background-image: url(<?php echo $main_img; ?>)">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'backgroundImagePanel', 'background_image_panel_shortcode' );







function image_content_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
        'align' => 'left',
        'overlap' => ""
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);

    $overlap = $a['overlap'];

    ?>
    <div class="imageContentPanel imageContentPanel-<?php echo $a['align'] ?> overlap-<?php echo $overlap; ?>">
        <div class="imageContentPanel__image-before" data-aos="fade-right">
            <div class="imageContentPanel__image" style="background-image: url('<?php echo $main_img; ?>')"></div>
        </div>

        <div class="imageContentPanel__info">
            <div class="imageContentPanel__info-inner" data-aos="fade-left">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'imageContentPanel', 'image_content_panel_shortcode' );