<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function generateRandomString(){
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($permitted_chars), 0, 12);
}


/**
 * Short Code for responsive spacer
 */
function gp_spacer_shortcode ( $atts ){
    $a = shortcode_atts( array(
        'height' => '30',
    ), $atts);
    $rand = generateRandomString();
    ob_start();
    ?>
    <style>
        .gp-responsive-spacer-<?php echo $rand; ?> {
            padding-bottom: <?php echo (int) $a['height'] * 1;?>px;
        }
        @media screen and (max-width: 1024px){

            .gp-responsive-spacer-<?php echo $rand; ?> {
                padding-bottom: <?php echo (int) $a['height'] * .5; ?>px;
            }
        }
        @media screen and (max-width: 700px){
            .gp-responsive-spacer-<?php echo $rand; ?> {
                padding-bottom: <?php echo (int) $a['height'] * .4; ?>px;
            }
        }
    </style>
    <div class="gp-responsive-spacer-<?php echo $rand; ?>"></div>

    <?php
    $output = ob_get_clean();

    return $output;
}
add_shortcode('spacer', 'gp_spacer_shortcode');