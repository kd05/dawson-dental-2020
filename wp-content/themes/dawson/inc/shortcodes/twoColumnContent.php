<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function twoColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="twoColumnContent">
        <div class="twoColumnContent__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'twoColumnContent', 'twoColumnContent_shortcode' );




function singleColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
            "animation" => "right"
    ), $atts );
    ob_start();
    ?>
    <div class="singleColumnContent"  <?php echo ' data-aos="fade-'.$a['animation'].'" ';  ?>>
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleColumnContent', 'singleColumnContent_shortcode' );
