<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function mobileButtons_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'text_1' => 'Virtual Consultation',
        'link_1' => '#',
        'icon_1' => 'fal fa-laptop',
        'text_2' => 'Make An Appointment',
        'link_2' => '#',
        'icon_2' => 'fal fa-comments-alt',
    ), $atts );
    ob_start();

    $text_1 = $a['text_1'];
    $link_1 = $a['link_1'];
    $icon_1 = $a['icon_1'];

    $text_2 = $a['text_2'];
    $link_2 = $a['link_2'];
    $icon_2 = $a['icon_2'];

    ?>
    <div class="mobileButtonsContainer ">
        <div class="mobileButtonsContainer__wrapper">
            <div class="mobileButtonsContainer--inner">
                <div class="mobileButtons__icon">
                    <i class="<?php echo $icon_1; ?>"></i>
                </div>
                <a href="<?php echo $link_1; ?>"><?php echo $text_1; ?></a>
            </div>
            <div class="mobileButtonsContainer--inner">
                <div class="mobileButtons__icon">
                    <i class="<?php echo $icon_2; ?>"></i>
                </div>
                <a href="<?php echo $link_2; ?>"><?php echo $text_2; ?></a>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'mobileButtons', 'mobileButtons_shortcode' );



