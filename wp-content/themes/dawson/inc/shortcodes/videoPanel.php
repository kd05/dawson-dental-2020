<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function video_panel_shortcode( $atts, $content = null ) {
    $a= array(
        'video_id' => '',
        'image_id' => '',
        'overlap' => 'yes'
    );
    $a = shortcode_atts($a, $atts);

    $id = (int)$a['video_id'];
    $video_url = wp_get_attachment_url($id);

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);

    $overlap = $a['overlap'];
    ob_start();

    ?>
    <div class="videoPanel videoPanel--overlap-<?php echo $overlap; ?>">
        <video loop muted autoplay poster="<?php echo $main_img; ?>" class="videoPanel__video">
            <source src="<?php echo $video_url; ?>" type="video/mp4">
        </video>
        <div class="videoPanel__gradient"></div>
        <div class="videoPanel__content">
            <div class="videoPanel__content-wrapper"  data-aos="fade-up" >
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'videoPanel', 'video_panel_shortcode' );