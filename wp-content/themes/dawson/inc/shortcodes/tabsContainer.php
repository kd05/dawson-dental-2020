<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function tabsContainer_shortcode( $atts, $content = null ) {
    $a= array(
        'container' => '',
        'active' => ''
    );
    $a = shortcode_atts($a, $atts);

    $container = $a['container'];
    $active = $a['active'];
    ob_start();
    ?>
    <div class="tabsContainer <?php echo $container." ".$active; ?>">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'tabsContainer', 'tabsContainer_shortcode' );





function tabsButtonContainer_shortcode( $atts, $content = null ) {
    $a= array(
    );
    ob_start();

    ?>
    <div class="tabsButtonContainer">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'tabsButtonContainer', 'tabsButtonContainer_shortcode' );





function tabButton_shortcode( $atts, $content = null ) {
    $a= array(
        'tab' => '',
        'text' => 'READ MORE',
        'link' => 'javascript:void(0)',
        'width' => 'auto',
        'active' => ''
    );
    $a = shortcode_atts($a, $atts);
    ob_start();

    $tab = $a['tab'];;
    $color = "blueButton";
    $text = $a['text'];
    $link = $a['link'];
    $width = $a['width'];
    $active = $a['active'];
    ?>
    <div class="tabButton buttonContainer <?php echo $color." ".$tab;?>">
        <a class="<?php echo $active; ?>" href="<?php echo $link; ?>" style="min-width: <?php echo $width; ?>" data-tab="<?php echo $tab; ?>"><?php echo $text; ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'tabButton', 'tabButton_shortcode' );