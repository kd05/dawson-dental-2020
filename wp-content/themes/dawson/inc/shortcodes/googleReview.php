<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function googleReview_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '197',
    ), $atts );
    ob_start();

    $logo_id = $a['image_id'];
    $logo_img = current(wp_get_attachment_image_src($logo_id, "medium"));

    ?>
    <div class="googleReviewContainer">
        <div class="googleReviewContainer__inner"  data-aos="fade-up">
            <div class="googleReview--logo" >
                <img src="<?php echo $logo_img ?>" alt="">
            </div>
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'googleReview', 'googleReview_shortcode' );