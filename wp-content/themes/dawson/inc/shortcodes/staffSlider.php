<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function staffSlider_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'ids' => '223,230,231,232',
    ), $atts );
    ob_start();

    $staff_ids = $a['ids'];
    $staffMembers = explode(",", $staff_ids);


    ?>
    <div class="staffSliderContainer">

        <div class="centerContentContainer">
            <div class="centerContent"  data-aos="fade-up">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>

        <div class="staffSliderContainer--before"  data-aos="fade-up">
            <div class="staffSlider frame"  >
                <ul>
                <?php
                    foreach ($staffMembers as $staffMemberId){
                        $staffMemnberImg = get_the_post_thumbnail_url($staffMemberId, 'large');
                        $content = get_post_content($staffMemberId);
                        $staffMemberName = get_field("staff_name",$staffMemberId);
                        $staffMemberEducation = get_field("staff_education",$staffMemberId);

                        ?>
                            <li class="staffSlider--single" >
                                <div class="staffSlider--single__content">
                                    <div class="staff-img" style="background-image: url(<?php echo $staffMemnberImg; ?>)"></div>
                                    <div class="staff-info">
                                        <div class="staff-info--content">
                                            <h1><?php echo $staffMemberName; ?>, <span><?php echo $staffMemberEducation; ?></span></h1>
                                            <?php echo $content; ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                    }
                ?>
                </ul>
            </div>
            <div class="scrollbar common-scrollbar"  >
                <div class="handle">
                    <div class="mousearea"></div>
                </div>
            </div>

            <div class="slySliderButton">
                <i class="far fa-caret-left sly-prev"></i>
                <i class="far fa-caret-right sly-next"></i>
            </div>

        </div>

    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'staffSlider', 'staffSlider_shortcode' );
