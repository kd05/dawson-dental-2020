<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function footer_content_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="footerContent" >
            <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'footerContent', 'footer_content_shortcode' );





function footer_content_left_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "button_text" => "Find Your Closest Location",
        "button_link"   => "#"
    ), $atts );
    ob_start();

    $color = "white";
    $text = $a['button_text'];
    $link = $a['button_link'];
    ?>
        <div class="footerContent__left"  data-aos="fade-right" >
            <?php echo do_shortcode($content); ?>
            <?php
            wp_nav_menu(array(
                'container'      => false,
                'theme_location' => 'social-nav'
            ));
            ?>
            <?php echo do_shortcode("[button color='".$color."' text='".$text."' link='".$link."']");  ?>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'footerContent_left', 'footer_content_left_shortcode' );





function footer_content_right_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();

    ?>
        <div class="footerContent__right">
            <div class="footerContent__right--box"  data-aos="fade-left" >
                <?php echo do_shortcode($content); ?>

                <div class="footerFormContainer common-form form">
                    <?php // echo do_shortcode('[contact-form-7 id="268" title="Footer - Contact Form"]'); ?>
                    <?php //echo do_shortcode('[wpforms id="301"]'); ?>
                    <?php echo do_shortcode('[wpforms id="300"]'); ?>
                </div>
            </div>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'footerContent_right', 'footer_content_right_shortcode' );






function footer_form_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();

    ?>

    <div class="footerFormContainer common-form form">
        <!-- Submit button-->
            <div class="input-wrapper input-flex-wrapper">
                <div class="f-icon">
                    <i class="far fa-pencil-alt"></i>
                </div>
                <input type="text"  class="required" placeholder="Full Name"  name="full-name" id="full-name" value="">
            </div> <!-- /input-wrapper -->

            <div class="input-wrapper input-flex-wrapper">
                <div class="f-icon">
                    <i class="far fa-at"></i>
                </div>
                <input type="email"  class="required chk-email" placeholder="Email" name="email-address" id="email-address" value="">
            </div> <!-- /input-wrapper -->

            <div class="input-wrapper input-flex-wrapper">
                <div class="f-icon">
                    <i class="far fa-phone"></i>
                </div>
                <input type="text"  class="required" placeholder="Phone Number" name="phone-no" id="phone-no" value="">
            </div> <!-- /input-wrapper -->

            <div class="input-wrapper input-flex-wrapper">
                <div class="f-icon">
                    <i class="far fa-comments-alt"></i>
                </div>
                <input type="text"  class="required" placeholder="Extra Details"  name="extra-detail" id="extra-detail" value="">
            </div> <!-- /input-wrapper -->

            <div class="button-wrapper input-flex-wrapper">
                <input type="submit"  value="BOOK NOW">
            </div> <!-- /input-wrapper -->
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'footerForm', 'footer_form_shortcode' );
