<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function three_blue_box_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "button_text" => "",
        "button_link"   => "#"
    ), $atts );
    ob_start();

    $color = "yellow";
    $text = $a['button_text'];
    $link = $a['button_link'];
    ?>
    <div class="threeBlueBoxPanel">
        <div class="threeBlueBoxPanel__wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
        <?php
            if($text != ""){
                echo do_shortcode("[button color='".$color."' text='".$text."' link='".$link."']");
            }
        ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'threeBlueBoxPanel', 'three_blue_box_panel_shortcode' );




function single_blue_box_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
        'heading' => '',
        'title' => 'Dicolored, misaligned or broken teeth',
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);

    $heading = trim($a['heading']);
    $title = $a['title'];


    ?>
    <div class="singleBlueBox">
        <div class="singleBlueBox__content"  >
            <div class="singleBlueBox__content--inner">
                <div class="singleBlueBox__content_image">
                    <img src="<?php echo $main_img; ?>" alt="">
                </div>
                <?php if(!empty($heading)) { echo "<h4>$heading</h4>"; } ?>
                <p><?php echo $title; ?></p>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'singleBlueBox', 'single_blue_box_shortcode' );
