<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function blueImageOverlayContent_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => ''
    );
    $a = shortcode_atts($a, $atts);


    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    ob_start();

    ?>
    <div class="blueImageOverlayContent">
        <div class="blueImageOverlayContent--inner"  style="background-image: url(<?php echo $main_img; ?>)">
            <div class="blueImageOverlayContent__gradient"></div>
            <div class="blueImageOverlayContent__content">
                <div class="blueImageOverlayContent__content-wrapper"  data-aos="fade-up" >
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'blueImageOverlayContent', 'blueImageOverlayContent_shortcode' );