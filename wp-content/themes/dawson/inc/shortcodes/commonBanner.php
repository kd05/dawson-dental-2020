<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function commonBanner_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '124',
        "type" => "home"
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img);

    $bannerType = "commonBannerSection__".$a['type'];

    ?>
    <div class="commonBannerSection <?php echo $bannerType; ?>" >
        <div class="commonBannerSection__image" style="background-image: url(<?php echo $main_img; ?>)"></div>
        <div class="commonBannerSection__leftGradient"></div>
        <div class="commonBannerSection__bottomGradient"></div>

        <div class="commonBannerSection__info" >
            <div class="commonBannerSection__info__wrapper"  data-aos="fade-up" >
                <?php echo do_shortcode($content); ?>
            </div>            
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'commonBanner', 'commonBanner_shortcode' );