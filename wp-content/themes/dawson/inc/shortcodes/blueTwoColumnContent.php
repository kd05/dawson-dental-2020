<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function blueTwoColumnContent_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="blueTwoColumnContent"  data-aos="fade-right">
        <div class="blueTwoColumnContent__wrapper">
                <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'blueTwoColumnContent', 'blueTwoColumnContent_shortcode' );




function blueTwoColumnContent_Left_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
    ), $atts );
    ob_start();
    ?>
    <div class="blueTwoColumnContent_Left">
        <div class="blueTwoColumnContent_Left--inner">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'blueTwoColumnContent_Left', 'blueTwoColumnContent_Left_shortcode' );



function blueTwoColumnContent_Right_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image_id' => '250',
        'youtube_id' => ''
    ), $atts );
    ob_start();

    $main_img = $a['image_id'];
    $main_img = current(wp_get_attachment_image_src($main_img, "large"));

    $youtube_id = $a['youtube_id'];
    $video_a_tag = "";
    if($youtube_id){
        $data_video_id =  " data-video-id='$youtube_id' ";
        $video_a_tag = "
    <img class='playBtn' src='".get_template_directory_uri()."/assets/images/play-btn.png' alt=''>
<a class='js-modal-video' href='javascript:void(0);' $data_video_id></a>";
    }
    ?>
    <div class="blueTwoColumnContent_Right">
        <div class="blueTwoColumnContent_Right--inner">
            <div class="blueTwoColumnContent_Right--image"  style="background-image: url(<?php echo $main_img; ?>)">
                <?php  echo $video_a_tag; ?>
            </div>
            <div class="blueTwoColumnContent_Right--content">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'blueTwoColumnContent_Right', 'blueTwoColumnContent_Right_shortcode' );