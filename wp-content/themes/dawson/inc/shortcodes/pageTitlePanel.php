<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function pageTitlePanel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        "is_image" => "no",
        "image_id" => "44",
        "max_width" => ""
    ), $atts );
    ob_start();

    $is_image = $a['is_image'];
    $image_id = $a['image_id'];
    $max_width = $a['max_width'];
    $image_array = wp_get_attachment_image_src($image_id, "full");
    $image_src = ($is_image == "yes" && is_array($image_array)) ? current($image_array) : "";

    $class = "";
    if($max_width == "half"){
        $class = "content-half";
    }


    ?>
        <div class="pageTitlePanel pageTitlePanel--image-<?php echo $is_image; ?>"  style="background-image: url(<?php echo $image_src; ?>)">
            <div class="pageTitlePanel__content <?php echo $class; ?>" >
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'pageTitlePanel', 'pageTitlePanel_shortcode' );
