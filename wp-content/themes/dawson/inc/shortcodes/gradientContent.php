<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function gradientContent_shortcode( $atts, $content = null ) {
    $a= array(
        'image_id' => '',
        'align' => 'left'
    );
    $a = shortcode_atts($a, $atts);


    $main_img = $a['image_id'];
    $main_img = wp_get_attachment_url($main_img, "large");
    $align = $a['align'];
    ob_start();

    ?>
    <div class="gradientContent gradientContent--<?php echo $align; ?>">
        <div class="gradientContent--inner"  style="background-image: url(<?php echo $main_img; ?>)">
            <div class="gradientContent__gradient"></div>
            <div class="gradientContent__gradient--horizontal"></div>
            <div class="gradientContent__content">
                <div class="gradientContent__content-wrapper"  data-aos="fade-up" >
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gradientContent', 'gradientContent_shortcode' );



function li_check_shortcode( $atts, $content = null ) {
    ob_start();
    ?>
        <div class="listStyle__check"  >
            <?php echo do_shortcode($content); ?>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'li_check', 'li_check_shortcode' );