<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



// Register menus
register_nav_menus(
    array(
        'main-nav'      => __('The Main Menu', 'dawson'),
        'top-nav'      => __('The Top Menu', 'dawson'),
        'footer-nav'   => __('Footer Menu', 'dawson'),
        'social-nav'   => __('Social Menu', 'dawson'),
    )
);

