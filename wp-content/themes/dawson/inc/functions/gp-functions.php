<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





/**
 * Generates an excerpt of the desired text.
 * If no second paramter is passed then it will generate an excerpt 20 words long.
 * If any words are cut off by the excerpt then ( ... ) will be appended to the text.
 * Returns a string.
 *
 * @param string $content text that you would like an excerpt of
 * @param int $num number of words to contain in excerpt
 *
 * @return string
 */
function gp_excerptize($content, $num = 30)
{
    $number = $num;
    //$content = apply_filters('the_content', $content);
    //echo "<pre style='display: none;'>".print_r($content, true)."</pre>";
    // $content = strip_tags( $content, '<br>' );
    $content = strip_tags($content, '');
    $content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $content);
    $content = str_replace('&nbsp;', '', $content);

    $contentArray = explode(' ', $content, $number + 1);
    //echo '<pre>'.print_r($contentArray, true).'</pre>';
    $contentString = '';
    foreach ($contentArray as $key => $value) {
        if ($key >= $number) {
            $contentString .= '...';
            break;
        }
        $contentString .= trim($value);
        if ($key < $number - 1) {
            $contentString .= ' ';
        }
    }

    return $contentString;
}

function gp_get_img_url( $image_id, $size = 'large' ) {
    $src = wp_get_attachment_image_src( $image_id, $size, false );
    $url = isset( $src[ 0 ] ) ? $src[ 0 ] : false;
    return $url;
}


function get_svg_path( $svgId){
    $svgId = (int) $svgId;
    $svgArray = wp_get_attachment_metadata($svgId);
    $svgFile = $svgArray['file'];
    $uploadBaseDir =  wp_upload_dir()['basedir'];

    return $svgFile ?  $uploadBaseDir.$svgFile : false;
}



function get_post_content($post_id){
    $p = get_post($post_id);
    return apply_filters('the_content', $p->post_content);
}



function get_default_banner(){
    return get_template_directory_uri()."/assets/images/home-banner.jpg";
}



function pagination_bar( $custom_query ) {

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}