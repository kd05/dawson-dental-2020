<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function global_scripts()
{
    //    Google fonts
    wp_enqueue_style('montserrat-font', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap');

    //    jQuery UI
    wp_enqueue_style( 'jquery-ui-style', get_template_directory_uri() . '/assets/styles/dest/jquery-ui/jquery-ui.min.css' );

    wp_register_script('jquery-ui', get_template_directory_uri() . '/assets/scripts/dest/jquery-ui/jquery-ui.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'jquery-ui');

    //    Slick JS
    wp_register_script('sly-js', get_template_directory_uri() . '/assets/scripts/dest/sly/sly.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'sly-js');

    //    Slick JS
//    wp_register_script('slick-js', get_template_directory_uri() . '/assets/scripts/dest/slick/slick.min.js', array('jquery'), false, true );
//    wp_enqueue_script( 'slick-js');

    //    Slick Css
//    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/styles/dest/slick/slick.css' );
//    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/styles/dest/slick/slick-theme.css' );


    //    Modal Video JS
    wp_register_script('modal-video-js', get_template_directory_uri() . '/assets/scripts/dest/modal-video/jquery-modal-video.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'modal-video-js');

    //    Modal Video CSS
    wp_enqueue_style( 'modal-video-style', get_template_directory_uri() . '/assets/styles/dest/modal-video/modal-video.min.css' );


//    AOS Animation CSS
    wp_enqueue_style( 'aos-style', get_template_directory_uri() . '/assets/styles/dest/aos/aos.css' );
    //    AOS Animation JS
    wp_register_script('aos-js', get_template_directory_uri() . '/assets/scripts/dest/aos/aos.js', array('jquery'), false, true );
    wp_enqueue_script( 'aos-js');


    // Adding scripts file in the footer
    wp_register_script('moby-script', get_template_directory_uri() . '/assets/scripts/dest/moby/moby.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'moby-script');


    //    Main Css
    wp_enqueue_style('main-style', get_template_directory_uri() . '/assets/styles/dest/style.min.css', array(), false, 'all');

    //    Main JS
    wp_register_script('main-js', get_template_directory_uri() . '/assets/scripts/dest/main.min.js', array('jquery'), false, true);
    wp_enqueue_script( 'main-js');

    //    Font Awesome
    wp_enqueue_style('font-awesome5-pro', get_template_directory_uri() . '/assets/styles/dest/font-awesome/css/all.min.css', array(), false, 'all');

    $js_urls = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'theme_url' => get_template_directory_uri(),
        'image_url' => get_template_directory_uri()."/assets/images/"
    );
    wp_register_script('local_variables',"");
    wp_localize_script('local_variables', 'website_urls', $js_urls );
    wp_enqueue_script('local_variables');

}
add_action('wp_enqueue_scripts', 'global_scripts');