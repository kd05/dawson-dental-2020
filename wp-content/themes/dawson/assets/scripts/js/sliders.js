jQuery( document ).ready(function($) {


    const updateBeforeAfterSliderBgImage = (slider = 'beforeAfterSlider', number = 0) => {

        if($('.'+slider+' .beforeAfterSlider--single--'+number).length){

            let bgImg = $('.'+slider+' .beforeAfterSlider--single--'+number).attr('data-bg-img');
            console.log(`Here ${slider} Img: ${bgImg}`);
            $('.'+slider).siblings('.beforeAfterSliderContainer--abs').css('background-image', 'url('+bgImg+')');
            // $('.${slider} .beforeAfterSliderContainer--abs').css('background-image', 'url('+bgImg+')');
        }
    }



    const calcSlySliderWidth = (slider) => {
        if(slider == "locationSlider"){
            let slideWidth =  $('.locationSliderContainer .locationSliderContainer--before').width();
            // console.log(`locationSlider : ${slideWidth}`);
            $('.locationSlider--single').css('width', `${slideWidth}px`);
        }
        else if(slider == "staffSlider") {
            let slideWidth =  $('.staffSliderContainer .staffSliderContainer--before').width();
            // console.log(`staffSlider : ${slideWidth}`);
            $('.staffSlider--single').css('width', `${slideWidth}px`);
        }
        else if(slider == "beforeAfterSlider") {
            const screenWidth  = $( window ).width();
            const container =  $('.beforeAfterSliderContainer--abs--overlay').width();
            let slideWidth;
            if(screenWidth > 1024) {
                slideWidth  = Math.trunc((container * 83) / 100);
            } else {
                slideWidth  = Math.trunc((container * 96) / 100);
            }
            // console.log(`${slider} BeforeAfter slider width : ${slideWidth}`);
            $('.beforeAfterSlider--single').css('width', `${slideWidth}px`);
        }
        else{
            const screenWidth  = $( window ).width();
            const container =  $('.beforeAfterSliderContainer--abs--overlay').width();
            let slideWidth;
            if(screenWidth > 1024) {
                slideWidth  = Math.trunc((container * 83) / 100);
            } else {
                slideWidth  = Math.trunc((container * 96) / 100);
            }
            // console.log(`${slider} BeforeAfter slider width : ${slideWidth}`);
            $('.beforeAfterSlider--single').css('width', `${slideWidth}px`);
        }
    }


    const initializeSlySlider = (slider) => {
        setTimeout(() => {
            if ($(`.${slider}`).length) {

                calcSlySliderWidth(slider);

                let $frame = $(`.${slider}`);
                let $wrap  = $frame.parent();

                let options = {
                    horizontal: 1,
                    itemNav: 'forceCentered',
                    smart: 1,
                    activateMiddle: 1,
                    mouseDragging: 1,
                    touchDragging: 0,
                    releaseSwing: 0,
                    startAt: 0,
                    scrollBar: $wrap.find('.scrollbar'),
                    scrollBy: 0,
                    speed: 1000,
                    elasticBounds: 1,
                    easing: 'easeOutExpo',
                    dragHandle: 1,
                    dynamicHandle: 1,
                    clickBar: 1,

                    // Buttons
                    prev: $wrap.find('.sly-prev'),
                    next: $wrap.find('.sly-next')
                };

                // Call Sly on frame
                $frame.sly(options);


                if(slider != 'locationSlider' && slider != 'staffSlider'){
                    $frame.sly('on', 'active',  (eventName, sliderNumber) => {
                        updateBeforeAfterSliderBgImage(slider, sliderNumber);
                    });
                }



                $(window).resize(function(){
                    calcSlySliderWidth(slider);
                    $('.'+slider).sly('reload');
                });

            }
        }, 1);

    }



    initializeSlySlider('locationSlider');

    initializeSlySlider('staffSlider');


    const startSlider = (sliderCount) => {
        console.log(sliderCount);
        const num = sliderCount == 1 ? "" : sliderCount;
        initializeSlySlider('beforeAfterSlider'+num);
        updateBeforeAfterSliderBgImage('beforeAfterSlider'+num,0);
    };

    // For loop to work upto 8 before/After slider on single page (For Smile Gallery Page).
    for(let sliderCount = 1; sliderCount <= 8; sliderCount++){
        startSlider(sliderCount);
    }


});