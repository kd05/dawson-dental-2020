
jQuery( document ).ready(function($) {


    if($('.popupForm-overlay').length){

        //appends an "active" class to .popup and .popup-content when the "Open" button is clicked
        $(".openPopup").on("click", function() {
            const email = $(this).attr('data-email');
            const title = $(this).attr('data-title');
            if(typeof title != ""){
                $('.formTitle').html(title);
                $('#location-position').val(title);
            }
            if(typeof email === 'undefined' || email == ""){
                $('select#admin-email option:nth-child(1)').attr('selected', 'selected');
            } else{
                $('#admin-email').val(email);
            }
            console.log(email, title);
            setTimeout(() => {
                $(".popupForm-overlay, .popupForm-content").addClass("active");
            }, 100);
        });

        //removes the "active" class to .popup and .popup-content when the "Close" button is clicked
        $(".closePopup").on("click", function() {
            $(".popupForm-overlay, .popupForm-content").removeClass("active");
        });

    }



});