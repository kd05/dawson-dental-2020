
jQuery( document ).ready(function($) {

/**************************************************************************
    Form: Apply Blue color on field on focus and Black if it has value
************************************************************************ */
// Get all the forms in the page

// Currently disabling the color effect of the form after moving to wpforms
if(true){

    if($(".wpforms-container").length){

        const commonForm = document.querySelectorAll(".wpforms-container");
        const formArray = Array.from(commonForm);

        formArray.forEach((singleForm) => {
            const formInputs = singleForm.querySelectorAll('input[type=text], input[type=email], input[type=tel], select');
            // Add 'focusin' and 'focusout' event Each field in the form has
            Array.from(formInputs).forEach((formInput) => {

                formInput.setAttribute("autocomplete", "off");

                formInput.addEventListener('focusin', (e) => {
                    const parentElement = e.target.closest(".wpforms-field");
                    parentElement.classList.add('input-wrapper-blue');
                });
                formInput.addEventListener('focusout', (e) => {
                    const parentElement = e.target.closest(".wpforms-field");
                    const fieldValue = e.target.value.trim();
                    parentElement.classList.remove('input-wrapper-blue');
                    (fieldValue) ? parentElement.classList.add('input-wrapper-black') :
                        parentElement.classList.remove('input-wrapper-black');
                });
            });
        });

    }


}




});

