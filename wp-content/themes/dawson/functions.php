<?php




/***************************************************************
 *                          Inc / Functions
 **************************************************************/

require_once(get_template_directory() . '/inc/functions/enqueue-scripts.php');
require_once(get_template_directory() . '/inc/functions/menus.php');
require_once(get_template_directory() . '/inc/functions/hooks.php');
require_once(get_template_directory() . '/inc/functions/gp-functions.php');






/***************************************************************
 *                      Custom Post Types
 **************************************************************/

require_once(get_template_directory() . '/inc/customPostType/faqs/faqs.php');
require_once(get_template_directory() . '/inc/customPostType/careers/careers.php');
require_once(get_template_directory() . '/inc/customPostType/insurancefaqs/insurancefaqs.php');
require_once(get_template_directory() . '/inc/customPostType/location/location.php');
require_once(get_template_directory() . '/inc/customPostType/service/service.php');
require_once(get_template_directory() . '/inc/customPostType/staff/staff.php');
require_once(get_template_directory() . '/inc/customPostType/surgeryslider/surgeryslider.php');






/***************************************************************
 *                          Shortcodes
 **************************************************************/

require_once(get_template_directory() . '/inc/shortcodes/accordion.php');
require_once(get_template_directory() . '/inc/shortcodes/announcement.php');
require_once(get_template_directory() . '/inc/shortcodes/bannerBottomContent.php');
require_once(get_template_directory() . '/inc/shortcodes/beforeAfterSlider.php');
require_once(get_template_directory() . '/inc/shortcodes/blueBoxesPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/blueImageOverlayContent.php');
require_once(get_template_directory() . '/inc/shortcodes/blueTwoColumnContent.php');
require_once(get_template_directory() . '/inc/shortcodes/button.php');
require_once(get_template_directory() . '/inc/shortcodes/centerPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/commonBanner.php');
require_once(get_template_directory() . '/inc/shortcodes/footerContent.php');
require_once(get_template_directory() . '/inc/shortcodes/formWrap.php');
require_once(get_template_directory() . '/inc/shortcodes/googleReview.php');
require_once(get_template_directory() . '/inc/shortcodes/gradientContent.php');
require_once(get_template_directory() . '/inc/shortcodes/imageContentPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/locationMap.php');
require_once(get_template_directory() . '/inc/shortcodes/locationSlider.php');
require_once(get_template_directory() . '/inc/shortcodes/mobileButtons.php');
require_once(get_template_directory() . '/inc/shortcodes/pageTitlePanel.php');
require_once(get_template_directory() . '/inc/shortcodes/popupForm.php');
require_once(get_template_directory() . '/inc/shortcodes/servicePanel.php');
require_once(get_template_directory() . '/inc/shortcodes/servicesFourColumnBox.php');
require_once(get_template_directory() . '/inc/shortcodes/spacer.php');
require_once(get_template_directory() . '/inc/shortcodes/staffSlider.php');
require_once(get_template_directory() . '/inc/shortcodes/tabsContainer.php');
require_once(get_template_directory() . '/inc/shortcodes/threeBlueBoxPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/twoColumnContent.php');
require_once(get_template_directory() . '/inc/shortcodes/videoPanel.php');

