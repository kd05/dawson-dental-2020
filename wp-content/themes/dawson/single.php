<?php
/**
 * singular page - blog post, default page template, etc.
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( have_posts() )
    the_post();

// setup globals, config, if needed, and include everything else.
// gp_set_global( 'container_class', 'has-x-padding has-y-padding' );

get_header();

global $post;
$featured_img = get_post_thumbnail_id();
$img_url = gp_get_img_url($featured_img, 'full');

$title = get_the_title();

?>



<?php

$page_header = get_the_title();
$is_bg_image = "yes";
$bg_image_id = get_post_thumbnail_id();


if(is_singular("post")){

    $image_array = wp_get_attachment_image_src($bg_image_id, "full");
    $image_src = (is_array($image_array)) ? current($image_array) : "";
    ?>


    <div class="singlePostHeader " >
        <div class="singlePostHeader__content" >
            <div class="singlePostHeader__wrapper">
                <div class="singlePostHeader--title">
                    <h1><?php echo $page_header; ?></h1>
                </div>

                <div class="singlePostHeader--img"  style="background-image: url(<?php echo $image_src; ?>)">

                </div>
            </div>
        </div>
    </div>

    <?php

} else {

    echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
        ."<h1>".$page_header."</h1>".
        "[/pageTitlePanel]");
}



?>


<div id="<?php echo $post->ID; ?> post-content" <?php post_class ('page-wrapper'); ?>>
    <?php echo the_content(); ?>
</div>


<?php
get_footer();
