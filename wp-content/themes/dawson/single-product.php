<?php
/**
 * singular page - blog post, default page template, etc.
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( have_posts() )
    the_post();

// setup globals, config, if needed, and include everything else.
// gp_set_global( 'container_class', 'has-x-padding has-y-padding' );

get_header();

global $post;
$featured_img = get_post_thumbnail_id();
$img_url = gp_get_img_url($featured_img, 'full');

$title = get_the_title();

?>



<?php
$postType = get_post_type();

if($postType == "product"){
    $subHeading = "<p>Pay your invoice or account balance online below. 
Please select your Dawson location from the drop down below, and enter your Account/Chart number along with the amount owing. 
Please note: Visa Debit Cards are not accepted for credit card payments. </p>";
} else {
    $subHeading = "";
}


$page_header = get_the_title();
//$is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
//$bg_image_id = get_field("background_image") ? get_field("background_image") : "";
$is_bg_image = "no";
$bg_image_id = get_post_thumbnail_id();
echo do_shortcode("[pageTitlePanel max_width='half' is_image='$is_bg_image' image_id='$bg_image_id']"
    ."<h1>".$page_header.$subHeading."</h1>".
    "[/pageTitlePanel]");


?>


<div id="<?php echo $post->ID; ?> post-content" <?php post_class ('page-wrapper'); ?>>
    <?php echo the_content(); ?>
</div>


<?php
get_footer();
