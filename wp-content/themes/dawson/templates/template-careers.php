<?php
/*
 * Template Name: Career's Page
 * description: Career's Page
 */

get_header();

$faqCategories = get_terms( array(
    'taxonomy' => 'faq-types',
    'hide_empty' => false,
) );

?>

    <section>

            <?php
            $page_header = get_field("page_header");
            $is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
            $bg_image_id = get_field("background_image") ? get_field("background_image") : "";

            echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
                                            .$page_header.
                                       "[/pageTitlePanel]");

            ?>

            <div class="careerContainer margin-top-125">
                <div class="careerContainer__wrapper">
                    <div class="careerLeftSection">
                        <div class="careerLeftSection--menu">

                            <h3><span class="blueFont">General</span> Application</h3>
                            <p>Don't see a position that suits you? Submit your resume to be notified about future openings.</p>

                            <?php  echo do_shortcode('[contact-form-7 id="18977" title="Careers"]'); ?>

                        </div>
                    </div>

                    <div class="careerRightSection">
                        <div class="careerRightSection--inner">

                                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                                    <?php the_content(); ?>

                                <?php endwhile; endif; ?>


                                <?php
                                $locations = get_posts(
                                    array(
                                        'fields' => 'ids',
                                        'posts_per_page' => -1,
                                        'post_type' => 'dawsoncareers',
                                        'post_status' => 'publish',
                                        'orderby' => 'menu_order',
                                        'order' => 'ASC'
                                    )
                                );

                                ?>
                                <div class="careerCategoryContainer">
                                    <?php
                                    foreach ($locations as $locationID){
                                        $title = get_the_title($locationID);
                                        $content = get_post_content($locationID);
                                        ?>
                                        <div class="commonAccordionContainer">
                                            <div class="accordionTitle">
                                                <h4><?php echo $title; ?></h4>

                                            </div>
                                            <div class="accordionContent">
                                                <?php echo $content; ?>

                                            </div>
                                        </div>
                                        <?php
                                    } ?>

                                </div>



                            </div>
                    </div>

                </div>
            </div>


    </section>

<?php  echo do_shortcode('[popupForm]'); ?>

<?php get_footer(); ?>