<?php
/*
 * Template Name: FAQ's Page
 * description: FAQ's Page
 */

get_header();

$faqCategories = get_terms( array(
    'taxonomy' => 'faq-types',
    'hide_empty' => false,
) );

?>

    <section>

            <?php
            $page_header = get_field("page_header");
            $is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
            $bg_image_id = get_field("background_image") ? get_field("background_image") : "";

            echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
                                            .$page_header.
                                       "[/pageTitlePanel]");

            ?>

            <div class="faqContainer margin-top-125">
                <div class="faqContainer__wrapper">
                    <div class="faqLeftSection">
                        <div class="faqLeftSection--menu">
                            <ul>
                                <?php
                                    foreach ($faqCategories as $faqCategory) {
                                        $catName = $faqCategory->name;
                                        $catSlug = "#". $faqCategory->slug;
                                        echo "<li><a href='$catSlug'>$catName</a></li>";
                                        
                                    }
                                ?>
                            </ul>
                            

                        </div>
                    </div>
                    <div class="faqRightSection">
                        <div class="faqRightSection--inner">
                            <?php
                            foreach ($faqCategories as $faqCategory) {
                                $catId = $faqCategory->term_id;
                                $catName = $faqCategory->name;
                                $catSlug = $faqCategory->slug;
                                $faqs = get_posts(
                                    array(
                                        'fields' => 'ids',
                                        'posts_per_page' => -1,
                                        'post_type' => 'gpfaq',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'faq-types',
                                                'field' => 'term_id',
                                                'terms' => $catId,
                                            )
                                        )
                                    )
                                );

                                ?>
                                    <div class="faqCategoryContainer">
                                        <h3 id="<?php  echo $catSlug; ?>"><?php  echo $catName; ?></h3>

                                        <?php
                                        foreach ($faqs as $faqID){
                                            $faqTitle = get_the_title($faqID);
                                            $faqContent = get_post_content($faqID);
                                            ?>
                                            <div class="commonAccordionContainer">
                                                <div class="accordionTitle">
                                                    <h4><?php echo $faqTitle; ?></h4>

                                                </div>
                                                <div class="accordionContent">
                                                    <?php echo $faqContent; ?>
                                                </div>
                                            </div>
                                        <?php
                                        } ?>

                                    </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>


    </section>



<?php get_footer(); ?>