<?php
/*
 * Template Name: Inner White Box Page
 * description: Inner White Box Page
 */

get_header();

?>

    <section>

            <?php
            $page_header = get_field("page_header");
            $is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
            $bg_image_id = get_field("background_image") ? get_field("background_image") : "";

            echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
                                            .$page_header.
                                       "[/pageTitlePanel]");

            ?>

            <div class="innerPageWhiteBox margin-top-125">
                    <div class="innerPageWhiteBox--inner">

                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <?php the_content(); ?>

                        <?php endwhile; endif; ?>

                    </div>
            </div>


    </section>



<?php get_footer(); ?>