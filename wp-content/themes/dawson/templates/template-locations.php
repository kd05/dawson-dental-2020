<?php
/*
 * Template Name: Locations Listing Page
 * description: Locations Listing Page
 */

get_header();

?>

<?php
$page_header = get_field("page_header");
$is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
$bg_image_id = get_field("background_image") ? get_field("background_image") : "";

echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
    .$page_header.
    "[/pageTitlePanel]");

?>

    <div class="location-section">

        <div class="storeLocatorContainer" data-aos="fade-up">
            <?php  echo do_shortcode("[wpsl]"); ?>
        </div>



        <div class="location-wrapper">
            <div class="location-flex">

                <?php
                $args = array(
                        'fields' => 'ids',
                        'posts_per_page' => -1,
                        'post_type' => 'gp-location',
                        'post_status' => 'publish',
                        'orderby' => 'menu_order',
                        'order' => 'ASC'
                );
                $locations = get_posts($args);


                foreach ($locations as $locationID){
                        $title = get_field("location_title", $locationID);
                        $location_address = nl2br( get_field("location_address", $locationID));
                        $phone_number = get_field("phone_number", $locationID);
                        $image = get_the_post_thumbnail_url($locationID, "large");
                        $link = get_permalink($locationID);
                        ?>
                        <div class="location-listing entry-content"  data-aos="fade-up" >
                            <div class="location-img">
                                <a href="<?php echo $link; ?>"></a>
                                <div class="background-image standard" style="background-image: url(<?php echo $image; ?>)"></div>
                            </div>
                            <div class="location-title"><a href="<?php echo $link; ?>"><h3><?php echo $title; ?></h3></a></div>
                            <div class="location-excerpt"><?php echo substr($location_address, 0, 115); ?></div>
                            <div class="location-phone"><?php echo substr($phone_number, 0, 115); ?></div>
                            <div class="buttonContainer yellowButton left-align">
                                <a href="<?php echo $link; ?>" style="min-width: auto">Learn More</a>
                            </div>
                        </div>
                        <?php
                }
                ?>
            </div>
<!--            <nav class="gp-pagination">-->
<!--                --><?php //pagination_bar( $query ); ?>
<!--            </nav>-->




        </div>
    </div>



<?php get_footer(); ?>