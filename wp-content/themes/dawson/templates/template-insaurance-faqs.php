<?php
/*
 * Template Name: Insurance FAQ's Page
 * description: Insurance FAQ's Page
 */

get_header();

?>

    <section>

            <?php
            $page_header = get_field("page_header");
            $is_bg_image = get_field("is_background_image") ? get_field("is_background_image") : "no";
            $bg_image_id = get_field("background_image") ? get_field("background_image") : "";

            echo do_shortcode("[pageTitlePanel is_image='$is_bg_image' image_id='$bg_image_id']"
                                            .$page_header.
                                       "[/pageTitlePanel]");

            ?>

            <div class="faqInsuranceContainer margin-top-125">
                <div class="faqContainer__wrapper">

                    <div class="faqRightSection">
                        <div class="faqRightSection--inner">

                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <?php the_content(); ?>

                            <?php endwhile; endif; ?>


                            <?php
                                $faqs = get_posts(
                                    array(
                                        'fields' => 'ids',
                                        'posts_per_page' => -1,
                                        'post_type' => 'gpinsurancefaq',
                                        'post_status' => 'publish',
                                        'orderby' => 'menu_order',
                                        'order' => 'ASC'
                                    )
                                );

                                ?>
                                <div class="faqCategoryContainer">
                                    <?php
                                    foreach ($faqs as $faqID){
                                        $faqTitle = get_the_title($faqID);
                                        $faqContent = get_post_content($faqID);
                                        ?>
                                        <div class="commonAccordionContainer">
                                            <div class="accordionTitle">
                                                <h4><?php echo $faqTitle; ?></h4>

                                            </div>
                                            <div class="accordionContent">
                                                <?php echo $faqContent; ?>
                                            </div>
                                        </div>
                                    <?php
                                    } ?>

                                </div>

                        </div>
                    </div>

                </div>
            </div>


    </section>



<?php get_footer(); ?>