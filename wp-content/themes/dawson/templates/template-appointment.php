<?php
/*
 * Template Name: Appointment Booking Page
 * description: Appointment Booking Page
 */

get_header();
?>


    <section>

            <?php
            $page_header = get_field("page_header");
            echo do_shortcode("[pageTitlePanel]".$page_header."[/pageTitlePanel]")
            ?>

            <div class="appointmentPageContainer margin-top-125">
                <div class="appointmentPageContainer__wrapper">

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <?php the_content(); ?>

                    <?php endwhile; endif; ?>




<!--                    <div class="appointmentForm common-form form">-->
<!--                        <div class="appointmentForm__wrapper">-->
<!--                            <h3 class="blueFont">Preferred Time & Date</h3>-->
<!---->
<!--                            <div class="field-flex-wrapper">-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-calendar-alt"></i>-->
<!--                                        </div>-->
<!--                                        <input type="text"  class="required" placeholder="Date"  name="appo-date" id="appo-date" value="">-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-clock"></i>-->
<!--                                        </div>-->
<!--                                        <input type="text"  class="required" placeholder="Time" name="appo-time" id="appo-time" value="">-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!---->
<!--                            <h3 class="blueFont mt-50">Patient Details</h3>-->
<!---->
<!--                            <div class="field-flex-wrapper">-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-pencil-alt"></i>-->
<!--                                        </div>-->
<!--                                        <input type="text"  class="required" placeholder="Full Name"  name="appo-name" id="appo-name" value="">-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-phone"></i>-->
<!--                                        </div>-->
<!--                                        <input type="text"  class="required" placeholder="Phone Number" name="appo-phone" id="appo-phone" value="">-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!--                            <div class="field-flex-wrapper">-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-at"></i>-->
<!--                                        </div>-->
<!--                                        <input type="email"  class="required chk-email" placeholder="Email" name="appo-email" id="appo-email" value="">-->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper select-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="fal fa-map-marker-alt"></i>-->
<!--                                        </div>-->
<!---->
<!--                                        <select name="location_booking" id="location_booking">-->
<!--                                            <option value="">Location</option>-->
<!--                                            <option value="Aurora">Aurora</option>-->
<!--                                            <option value="Barrie">Barrie</option>-->
<!--                                        </select>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!---->
<!---->
<!--                            <div class="field-flex-wrapper">-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper select-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-question-circle"></i>-->
<!--                                        </div>-->
<!---->
<!--                                        <select name="location_booking" id="location_booking">-->
<!--                                            <option value="">New to Dawson?</option>-->
<!--                                            <option value="Yes">Yes</option>-->
<!--                                            <option value="No">No</option>-->
<!--                                        </select>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                                <div class="field-flex-wrapper__inner">-->
<!--                                    <div class="input-wrapper select-wrapper input-flex-wrapper">-->
<!--                                        <div class="f-icon">-->
<!--                                            <i class="far fa-tooth"></i>-->
<!--                                        </div>-->
<!---->
<!--                                        <select name="location_booking" id="location_booking">-->
<!--                                            <option value="">Type of Treatment</option>-->
<!--                                            <option value="Dental">Dental</option>-->
<!--                                            <option value="Other">Other</option>-->
<!--                                        </select>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!---->
<!--                            </div>-->
<!---->
<!---->
<!--                            <div class="input-wrapper input-flex-wrapper">-->
<!--                                <div class="f-icon">-->
<!--                                    <i class="far fa-comments-alt"></i>-->
<!--                                </div>-->
<!--                                <input type="text"  class="required" placeholder="Extra Details"  name="appo-detail" id="appo-detail" value="">-->
<!--                            </div>-->
<!---->
<!--                            <div class="button-wrapper input-flex-wrapper">-->
<!--                                <input type="submit" class="alignleft" value="BOOK NOW">-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->



                </div>
            </div>


    </section>


<?php get_footer(); ?>