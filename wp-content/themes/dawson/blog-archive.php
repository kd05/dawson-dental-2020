<?php

/**
 * Template Name: Blog Template
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


get_header();
?>




<?php
$allCategories = get_categories();

echo do_shortcode("[pageTitlePanel is_image='no' image_id='']"
    ."<h1>Blog</h1>".
    "[/pageTitlePanel]");
?>

    <div class="blog-section">
        <div class="blog-wrapper">


            <div class="blogCategorySelector">
                <select name="blogCatDropdown" id="blogCatDropdown"  onchange="javascript:location.href = this.value;">
                    <option value="<?php echo site_url() ?>/blog">All Categories</option>
                    <?php foreach ($allCategories as $singleCat) {
                        $catLink = get_term_link($singleCat->term_id);
                        $selected = ($cat_id == $singleCat->term_id) ? "selected" : "";
                        echo "<option value='$catLink'  $selected >$singleCat->name</option>";
                    } ?>
                </select>
            </div>

            <div class="blog-flex">
                <?php
                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        $args = array(
                            'post_status' => 'publish',
                            'post_type' => 'post',
                            'posts_per_page' => 6,
                            'paged' => $paged,
                        );
                        $posts = get_posts($args);
                        $query = new WP_Query($args);

                        if($posts) {
                            foreach($posts as $post) {
                                $categories= get_the_category($post->ID);
//                                $cat_name = $category[0]->name;
                                $date = get_the_time('m.d.Y', $post->ID);
                                $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
                                $title = get_the_title($post->ID);
                                $content = $post->post_content;
                                $text = apply_filters('the_content', $content);
                                $link = get_permalink($post->ID)
                                ?>
                                <div class="blog-listing entry-content">
                                    <div class="blog-img">
                                        <a href="<?php echo $link; ?>"></a>
                                        <div class="background-image standard" style="background-image: url(<?php echo $image; ?>)"></div>
                                    </div>
                                    <?php
                                    if(is_array($categories) && count($categories) > 0) {
                                        $totalCategories = count($categories);
                                     ?>
                                    <div class="blog-categories">
                                        <?php
                                            $cnt = 1;
                                            foreach ($categories as $cat){
                                                $catName = $cat->name;
                                                $catId = $cat->term_id;
                                                $catLink = get_term_link($catId);
                                                echo   "<a href='$catLink'>$catName</a>";
                                                if($cnt != $totalCategories) echo "<span> | </span>";
                                                $cnt++;
                                            }
                                        ?>
                                    </div>
                                    <?php  } ?>

                                    <div class="blog-title"><a href="<?php echo $link; ?>"><h3><?php echo $title; ?></h3></a></div>
                                    <div class="blog-date"><?php echo $date; ?></div>
                                    <div class="blog-excerpt"><?php echo gp_excerptize($text, 20); ?></div>
                                    <div class="buttonContainer yellowButton left-align">
                                        <a href="<?php echo $link; ?>" style="min-width: auto">Read More</a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
            </div>
            <nav class="gp-pagination">
                <?php pagination_bar( $query ); ?>
            </nav>
        </div>
    </div>

<?php
get_footer();
