<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Controls\Buttons;
use Bookly\Backend\Components\Controls\Inputs as ControlInputs;
use Bookly\Backend\Components\Settings\Inputs;
?>
<div class="tab-pane" id="bookly_settings_files">
    <form method="post" action="<?php echo esc_url( add_query_arg( 'tab', 'files' ) ) ?>">
        <?php Inputs::renderText( 'bookly_files_directory', __( 'Upload directory', 'bookly' ), __( 'Enter the network folder path where the files will be stored. If necessary, make sure that there is no free web access to the folder materials.', 'bookly' ) ); ?>
        <div class="panel-footer">
            <?php ControlInputs::renderCsrf() ?>
            <?php Buttons::renderSubmit() ?>
            <?php Buttons::renderReset() ?>
        </div>
    </form>
</div>