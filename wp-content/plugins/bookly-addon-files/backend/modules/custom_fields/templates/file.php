<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<li data-type="file">
    <div class="bookly-flexbox">
        <div class="bookly-flex-cell">
            <i title="<?php esc_attr_e( 'Reorder', 'bookly' ) ?>" class="bookly-js-handle bookly-icon bookly-icon-draghandle bookly-margin-right-sm bookly-cursor-move"></i>
        </div>
        <div class="bookly-flex-cell" style="width: 100%">
            <p><b><?php _e( 'File Upload Field', 'bookly' ) ?></b><a class="bookly-js-delete glyphicon glyphicon-trash text-danger bookly-margin-left-sm" href="#"
                title="<?php esc_attr_e( 'Remove field', 'bookly' ) ?>"></a></p>
            <div class="row">
                <div class="col-md-8">
                    <div class="input-group">
                        <input class="bookly-label form-control" type="text" value=""
                               placeholder="<?php esc_attr_e( 'Enter a label', 'bookly' ) ?>">
                        <label class="input-group-addon">
                            <input class="bookly-required" type="checkbox">
                            <span class="hidden-xs"><?php _e( 'Required field', 'bookly' ) ?></span>
                            <i class="visible-xs-inline-block glyphicon glyphicon-warning"></i>
                        </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php echo $services_html ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
</li>