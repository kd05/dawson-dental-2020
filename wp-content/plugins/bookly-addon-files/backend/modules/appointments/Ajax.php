<?php
namespace BooklyFiles\Backend\Modules\Appointments;

use Bookly\Lib as BooklyLib;
use BooklyFiles\Lib\Entities;

/**
 * Class Ajax
 * @package BooklyFiles\Backend\Modules\Appointments
 */
class Ajax extends BooklyLib\Base\Ajax
{
    /**
     * @inheritdoc
     */
    protected static function permissions()
    {
        return array( '_default' => 'user' );
    }

    /**
     * Disconnect file from Customer appointment.
     */
    public static function deleteCustomField()
    {
        $slug = self::parameter( 'slug' );
        $file = new Entities\Files();
        if ( $file->loadBy( compact( 'slug' ) ) ) {
            $file->deleteSafely();
        }
        wp_send_json_success();
    }
}