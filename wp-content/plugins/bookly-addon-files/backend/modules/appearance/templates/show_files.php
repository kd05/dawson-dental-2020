<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="col-md-3">
    <div class="checkbox">
        <label data-toggle="popover" data-trigger="hover" data-placement="auto" data-content="<?php echo esc_attr( 'Show custom fields required', 'bookly' ) ?>">
            <input type="checkbox" id="bookly-show-files" <?php checked( get_option( 'bookly_files_enabled' ) ) ?>>
            <?php esc_html_e( 'Show files', 'bookly' ) ?>
        </label>
    </div>
</div>