<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
use Bookly\Backend\Components\Appearance\Editable;
?>
<div class="bookly-box bookly-js-files"<?php if ( ! get_option( 'bookly_files_enabled' ) ) : ?> style="display: none;"<?php endif ?>>
    <div class="bookly-form-group">
        <label><?php _e( 'File', 'bookly' ) ?></label>
        <div>
            <div class="bookly-row">
                <div class="bookly-box">
                    <div class="bookly-btn bookly-inline-block">
                        <?php Editable::renderString( array( 'bookly_l10n_browse' ) ) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>