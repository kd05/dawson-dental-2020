jQuery(function($) {

    var
        $appointments_list = $('#bookly-appointments-list'),
        $dialog = $('#bookly-ca-attachments-dialog')
    ;

    $appointments_list
        .on('click', '[data-action=show-attachments]', function (e) {
            e.preventDefault();
            var dt = $appointments_list.DataTable(),
                data = dt.row($(this).closest('td')).data();
            $dialog.data('customer_appointment_id', data.ca_id).modal('show',{});
        });

    $dialog
        .on('show.bs.modal', function (e) {
            var ca_id = $dialog.data('customer_appointment_id');
            $.ajax({
                url :     ajaxurl,
                data:     {action: 'bookly_files_get_attachments', ca_id: ca_id, csrf_token : BooklyFilesL10n.csrf_token},
                dataType: 'json',
                success:  function (response) {
                    if (response.success) {
                        $('.modal-body', $dialog).html(response.data.html);
                    }
                }
            });

        })
        .on('hidden.bs.modal', function () {
            $('.modal-body', $dialog).html('<div class="bookly-loading"></div>');
        });
});