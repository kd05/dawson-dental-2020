<?php
namespace BooklyFiles\Backend\Components\Dialogs\Appointment\Attachments;

use Bookly\Lib as BooklyLib;

class Modal extends BooklyLib\Base\Component
{
    public static function render()
    {
        self::enqueueStyles( array(
            'bookly' => array( 'frontend/resources/css/ladda.min.css', )
        ) );

        self::enqueueScripts( array(
            'module' => array(
                'js/attachments-dialog.js' => array( 'jquery' ),
            ),
            'bookly' => array(
                'frontend/resources/js/spin.min.js'  => array( 'jquery' ),
                'frontend/resources/js/ladda.min.js' => array( 'jquery' ),
            )
        ) );

        self::renderTemplate( 'attachments_dialog' );
    }
}