<?php
namespace BooklyFiles\Backend\Components\Dialogs\Appointment\Edit\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Backend\Components\Dialogs\Appointment\Edit;

/**
 * Class Shared
 * @package BooklyFiles\Backend\Components\Dialogs\Appointment\Edit\ProxyProviders
 */
class Shared extends Edit\Proxy\Shared
{
    /**
     * Enqueue assets for AppointmentForm
     */
    public static function enqueueAssets()
    {
        self::enqueueScripts( array(
            'addon' => array(
                'frontend/modules/booking/resources/jquery.iframe-transport.js' => array( 'jquery', 'jquery-ui-widget' ),
                'frontend/modules/booking/resources/jquery.fileupload.js'       => array( 'bookly-jquery.iframe-transport.js' ),
            ),
            'module' => array(
                'js/customer-details.js' => array( 'bookly-jquery.fileupload.js' )
            ),
        ) );

        wp_localize_script( 'bookly-customer-details.js', 'BooklyFilesL10n', array(
            'form_data'    => array( 'action' => 'bookly_files_upload', 'csrf_token' => BooklyLib\Utils\Common::getCsrfToken() ),
            'csrf_token'   => BooklyLib\Utils\Common::getCsrfToken(),
            'ajaxurl'      => admin_url( 'admin-ajax.php' ),
        ) );
    }
}