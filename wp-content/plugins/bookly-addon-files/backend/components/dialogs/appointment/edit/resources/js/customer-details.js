jQuery(function ($) {
    $('body')
        .on('bookly.edit.customer_details', {},
            // Bin handler for customer details in Appointment Form.
            function (event, $container, customer) {
                var $file_upload = $container.find('.bookly-js-file-upload'),
                    $custom_field = $container.find('.bookly-js-file');
                if (customer.ca_id) {
                    $custom_field.data('ca_id', customer.ca_id);
                } else {
                    $container.find('.bookly-js-file-menu').hide();
                    $custom_field.removeData('ca_id');
                }
                $.each(customer.files, function (id, name) {
                    $container.find('#bookly-js-custom-fields > *[data-id="' + id + '"] .bookly-js-file-name').html(name);
                });
                $('.bookly-js-file', $container).trigger('change');
                $file_upload.fileupload({
                    url     : BooklyFilesL10n.ajaxurl,
                    dataType: 'json',
                    done: function (e, data) {
                        if (data.result.success) {
                            $(e.target).hide();
                            var $div = $(e.target).closest('div');
                            $div.find('.bookly-js-file').val(data.result.data.slug);
                            $div.find('.bookly-js-file-menu .bookly-js-file-name').html(data.result.data.name);
                            $div.find('.bookly-js-file-menu').show();
                            var field_id = $div.closest('.form-group').data('id');
                            if( customer.files === undefined ) {
                                customer['files'] = {};
                            }
                            customer['files'][field_id] = data.result.data.name;
                        }
                    }
                }).bind('fileuploadsubmit', function (e, data) {
                    data.formData = $.extend(
                        {custom_field_id: $(e.delegateTarget).closest('[data-type=file]').attr('data-id')},
                        BooklyFilesL10n.form_data
                    );
                });
            }
        )
        .on('click', '.bookly-js-custom-field-download', function () {
            var $container = $(this).closest('.form-group'),
                separator = BooklyFilesL10n.ajaxurl.indexOf('?') == -1 ? '?' : '&';
            window.open(
                BooklyFilesL10n.ajaxurl + separator + 'action=bookly_files_download&slug=' + $container.find('.bookly-custom-field').val() + '&csrf_token=' + BooklyFilesL10n.csrf_token,
                '_blank'
            );
        })
        .on('click', '.bookly-js-custom-field-delete', function () {
            var $custom_field = $(this).closest('.form-group').find('.bookly-js-file'),
                slug = $custom_field.val();
            $custom_field.val('').trigger('change');
            if ($custom_field.data('ca_id') == undefined) {
                $.ajax({
                    url  : BooklyFilesL10n.ajaxurl,
                    type : 'POST',
                    data : {
                        action:  'bookly_files_delete_custom_field',
                        slug  : slug,
                        csrf_token: BooklyFilesL10n.csrf_token
                    },
                    dataType : 'json'
                });
            }
        })
        .on('change', '.bookly-js-file', function () {
            var slug = $(this).val(),
                $container = $(this).closest('.form-group');
            $('.bookly-js-file-menu', $container).toggle(slug != '');
            $('.bookly-js-file-upload',$container).toggle(slug == '');
            if (slug == '') {
                $('span.bookly-js-file-name', $container).html('');
            }
        });
});