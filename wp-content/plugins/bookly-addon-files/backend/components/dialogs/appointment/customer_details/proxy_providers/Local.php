<?php
namespace BooklyFiles\Backend\Components\Dialogs\Appointment\CustomerDetails\ProxyProviders;

use Bookly\Backend\Components\Dialogs\Appointment\CustomerDetails\Proxy;

/**
 * Class Local
 * @package BooklyFiles\Backend\Components\Dialogs\Appointment\CustomerDetails\ProxyProviders
 */
class Local extends Proxy\Files
{
    /**
     * @inheritdoc
     */
    public static function renderCustomField( $custom_field )
    {
        self::renderTemplate( 'details', compact( 'custom_field' ) );
    }
}