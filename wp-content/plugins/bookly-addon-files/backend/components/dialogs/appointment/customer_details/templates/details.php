<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
    /** @var \stdClass $custom_field */
?>
<input id="custom_field_<?php echo esc_attr( $custom_field->id ) ?>" class="bookly-custom-field bookly-js-file" type="hidden">
<div class="bookly-js-file-menu row">
    <div class="bookly-js-file-name col-md-9"></div>
    <div class="col-md-3 text-right">
        <button type="button" class="btn btn-default btn-xs bookly-js-custom-field-download"><?php _e( 'download', 'bookly' ) ?></button>
        <button type="button" class="btn btn-link btn-xs bookly-js-custom-field-delete"><i class="text-danger glyphicon glyphicon-trash"></i></button>
    </div>
</div>
<input type="file" class="bookly-js-file-upload">