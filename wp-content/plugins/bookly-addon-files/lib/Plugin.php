<?php
namespace BooklyFiles\Lib;

use Bookly\Lib as BooklyLib;
use BooklyFiles\Backend\Components;
use BooklyFiles\Backend\Modules as Backend;
use BooklyFiles\Frontend\Modules as Frontend;

/**
 * Class Plugin
 * @package BooklyFiles\Lib
 */
abstract class Plugin extends BooklyLib\Base\Plugin
{
    protected static $prefix;
    protected static $title;
    protected static $version;
    protected static $slug;
    protected static $directory;
    protected static $main_file;
    protected static $basename;
    protected static $text_domain;
    protected static $root_namespace;
    protected static $embedded;

    /**
     * @inheritdoc
     */
    protected static function init()
    {
        if ( BooklyLib\Config::customFieldsActive() ) {
            // Init ajax.
            Backend\Appointments\Ajax::init();
            Components\Dialogs\Appointment\Attachments\Ajax::init();
            if ( get_option( 'bookly_files_enabled' ) ) {
                Frontend\Booking\Ajax::init();
            }

            // Init proxy.
            Backend\Appearance\ProxyProviders\Local::init();
            Backend\Appearance\ProxyProviders\Shared::init();
            Backend\Appointments\ProxyProviders\Local::init();
            Backend\Appointments\ProxyProviders\Shared::init();
            Backend\CustomFields\ProxyProviders\Local::init();
            Backend\Notifications\ProxyProviders\Shared::init();
            Backend\Settings\ProxyProviders\Shared::init();
            Components\Dialogs\Appointment\CustomerDetails\ProxyProviders\Local::init();
            Components\Dialogs\Appointment\Edit\ProxyProviders\Shared::init();
            if ( get_option( 'bookly_files_enabled' ) ) {
                Frontend\Booking\ProxyProviders\Local::init();
                Frontend\Booking\ProxyProviders\Shared::init();
            }
            Notifications\Assets\Item\ProxyProviders\Shared::init();
            Notifications\Assets\Test\ProxyProviders\Shared::init();
            ProxyProviders\Local::init();
            ProxyProviders\Shared::init();
        }
    }
}