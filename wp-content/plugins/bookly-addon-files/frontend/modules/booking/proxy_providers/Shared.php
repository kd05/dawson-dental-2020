<?php
namespace BooklyFiles\Frontend\Modules\Booking\ProxyProviders;

use Bookly\Lib as BooklyLib;
use Bookly\Frontend\Modules\Booking\Proxy;
use BooklyFiles\Lib\Plugin;

/**
 * Class Shared
 * @package BooklyFiles\Frontend\Modules\Booking\ProxyProviders
 */
class Shared extends Proxy\Shared
{
    /**
     * @inheritdoc
     */
    public static function enqueueBookingScripts( array $depends )
    {
        $version    = Plugin::getVersion();
        $bookly_url = plugins_url( '', Plugin::getMainFile() );

        wp_enqueue_script( 'bookly-iframe-transport', $bookly_url . '/frontend/modules/booking/resources/jquery.iframe-transport.js', array( 'jquery', 'jquery-ui-widget', 'bookly' ), $version );
        wp_enqueue_script( 'bookly-file-upload', $bookly_url . '/frontend/modules/booking/resources/jquery.fileupload.js', array( 'bookly-iframe-transport' ), $version );
        wp_enqueue_script( 'bookly-files', $bookly_url . '/frontend/modules/booking/resources/files.js', array( 'bookly-file-upload' ), $version );

        wp_localize_script( 'bookly-files', 'BooklyFilesL10n', array(
            'form_data'  => array( 'action' => 'bookly_files_upload', 'csrf_token' => BooklyLib\Utils\Common::getCsrfToken() ),
            'csrf_token' => BooklyLib\Utils\Common::getCsrfToken(),
            'ajaxurl'    => admin_url( 'admin-ajax.php' ),
        ) );

        return $depends;
    }
}