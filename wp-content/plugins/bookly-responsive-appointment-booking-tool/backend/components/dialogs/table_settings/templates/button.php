<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<div class="form-group text-right">
    <button type="button" class="btn btn-default bookly-btn-block-xs bookly-js-table-settings" data-table-name="<?php echo $table_name ?>" data-setting-name="<?php echo $setting_name ?>"><i class="fa fa-fw fa-cog"></i></button>
</div>